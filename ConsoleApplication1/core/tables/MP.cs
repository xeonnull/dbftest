using System;
using System.Collections.Generic;

namespace ConsoleApplication1.core.tables
{
    public class MP:DataFile<MPItem>
    {
        public MP(string path) : base(path)
        {
        }

        public override MPItem assignReqToStruct(Dictionary<string, string> req)
        {
            MPItem returnItem = new MPItem();

            returnItem.KVR = req["KVR"].Trim(' ');
            returnItem.MKB = req["MKB"].Trim(' ');
            returnItem.VISIT_CODE = req["VISIT_CODE"].Trim(' ');
            returnItem.START_DATE = DateTime.Parse(req["START_DATE"]);
            returnItem.FINAL_DATE = DateTime.Parse(req["FINAL_DATE"]);
            returnItem.AMOUNT = Math.Round(double.Parse(req["AMOUNT"]),2);
            returnItem.VISIT_SUM = Math.Round( double.Parse(req["VISIT_SUM"]),2);
            returnItem.ADD_SUM =  Math.Round(double.Parse(req["ADD_SUM"]),2);
            returnItem.TARIF_TYPE = Int32.Parse(req["TARIF_TYPE"]);
            returnItem.FIN_LEVEL = Int32.Parse(req["FIN_LEVEL"]);
            returnItem.ADD_CODE = Int32.Parse(req["ADD_CODE"]);
            returnItem.UNICUM = Int32.Parse(req["UNICUM"]);

            return returnItem;
        }
    }

   public class MPItem
    {
        public DateTime START_DATE;
        public DateTime FINAL_DATE;
        public string KVR;
        public string MKB;
        public string VISIT_CODE;
        public int TARIF_TYPE;
        public int FIN_LEVEL;
        public double AMOUNT;
        public double VISIT_SUM;
        public double ADD_SUM;
        public int ADD_CODE;
        public int UNICUM;
    }
}
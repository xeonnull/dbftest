using System;
using System.Collections.Generic;

namespace ConsoleApplication1.core.tables
{
    public class MT:DataFile<string>
    {
        public MT(string path) : base(path)
        {
        }

        public override string assignReqToStruct(Dictionary<string, string> req)
        {
            throw new NotImplementedException();
        }
  
    }

    public class MTItem
    {
        public string NOMPOD;
        public string AREA_CODE;
        public string OMS_NUM;
        public string RESULT;
        public string NOMK;
        public string NAME1;
        public string NAME2;
        public string NAME3;
        public string SEX;
        public DateTime BIRTH_DATE;
        public string DOC_TYPE;
        public string DOC_SER;
        public string DOC_NUM;
        public string RESIDENT;
        public string NASP_CODE;
        public string STRT_CODE;
        public string HOUSE;
        public string ROOM;
        public string NO_POLIS;
        public string SER_NUM;
        public string SOCIAL;
        public string PRIVILEGE;
        public string SS;
        public string FNASP_CODE;
        public string FSTRT_CODE;
        public string FHOUSE;
        public string FROOM;
        public int IN_TYPE;
        public int UNICUM;

    }
}
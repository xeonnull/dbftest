using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.IO;
using System.Runtime.CompilerServices;

namespace ConsoleApplication1.core
{
    public abstract class DataFile<T>
    {
        private OdbcConnection _connection;
        private string _file;

        public DataFile(string path)
        {
            _file = Directory.GetCurrentDirectory() + "\\" + path;
            _connection = new OdbcConnection();
            _connection.ConnectionString = "Driver={Microsoft dBase Driver (*.dbf)};SourceType=DBF;SourceDB=" +
                                           _file + ";Exclusive=No; NULL=NO;DELETED=NO;BACKGROUNDFETCH=NO;";
        }

        public abstract T assignReqToStruct(Dictionary<string, string> req);

        public T getItem(string[,] attrs)
        {
            var request = executeRequest();
            request = applyFilter(request, attrs);

            if (request.Count > 1)
            {
                throw new Exception("Wrong filter or corrupted handbook");
            }

            if (request.Count == 0)
            {
                string retStr = "";
                for (int i = 0; i < attrs.Length / 2; i++)
                {
                    retStr += " " + attrs[i,0] + ": " + attrs[i,1];
                }
                string text = File.ReadAllText("log.txt");
                string line = "Cannot find item with params"+retStr;
                File.WriteAllText("log.txt",text+Environment.NewLine+line);
                
                return default(T);
                //   throw new Exception("not found item in database");
            }

            return assignReqToStruct(request[0]);
        }

        public List<T> getItems(string[,] attrs)
        {
            List<T> returnList = new List<T>();

            var request = executeRequest();
            request = applyFilter(request, attrs);

            foreach (Dictionary<string, string> item in request)
            {
                returnList.Add(assignReqToStruct(item));
            }

            return returnList;
        }

        protected List<Dictionary<string, string>> applyFilter(List<Dictionary<string, string>> requestResult,
            string[,] filter)
        {
            List<Dictionary<string, string>> returnItems = new List<Dictionary<string, string>>();


            foreach (Dictionary<string, string> item in requestResult)
            {
                bool isValid = true;
                for (int attrKey = 0; attrKey < filter.Length / 2; attrKey++)
                {
                    if (item[filter[attrKey, 0]] != filter[attrKey, 1])
                        isValid = false;
                }

                if (isValid)
                    returnItems.Add(item);
            }

            return returnItems;
        }

        protected List<Dictionary<string, string>> executeRequest()
        {
            OdbcCommand command = _connection.CreateCommand();

            List<Dictionary<string, string>> result = new List<Dictionary<string, string>>();

            command.CommandText = "SELECT * FROM " + _file;
            _connection.Open();
            var item = command.ExecuteReader();
            //HEADERS
            var columns = new List<string>();

            for (int i = 0; i < item.FieldCount; i++)
            {
                columns.Add(item.GetName(i));
            }

            //VALUES
            Dictionary<string, string> temp;
            while (item.Read())
            {
                temp = new Dictionary<string, string>();
                for (int i = 0; i < item.FieldCount; i++)
                {
                    temp.Add(columns[i], item[i] + "");
                }

                result.Add(temp);
            }

            _connection.Close();
            return result;
        }
    }
}
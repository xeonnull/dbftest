using System;
using System.Collections.Generic;
using System.IO;
using ConsoleApplication1.core.tables;
using ConsoleApplication1.handbook;

namespace ConsoleApplication1.core.groups
{
    public class AGroup
    {
        private MA _ma;
        private AMP _mp;
        private AMT _mt;

        private string path;
        private string prefix;
        private PriceA price_A;
        
        public AGroup(string pathToExctractedData,string prefix,PriceA price)
        {
            path = pathToExctractedData;
            this.prefix = prefix;
            price_A = price;
            _ma = new MA("\\data\\"+prefix+"AMA.dbf");
            _mp = new AMP("\\data\\"+prefix+"AMP.dbf");
            _mt = new AMT("\\data\\"+prefix+"AMT.dbf");
        }

        public void ValidateAllItemsPrice(DateTime time)
        {
            foreach (var item in _mp.getItems(new string[,]{}))
            {
                CheckPrice(item);
            }
        }

        private void CheckPrice(AMPItem item)
        {
            var methodItem = _mt.getItem(new[,]
            {
                {"UNICUM",item.UNICUM+""}
            });
            
            var itemFnd = price_A.getItem(new[,]
            {
                {"SPEC_CODE",item.VISIT_CODE},
                {"TARIF_TYPE",item.TARIF_TYPE+""},
                {"LEVEL", item.FIN_LEVEL+""},
                {"METHOD",methodItem.METHOD},
                {"ADD_CODE",item.ADD_CODE+""}
            });
            if (itemFnd.SPEC_CODE == null)
                return;
            if (itemFnd.TARIF != item.VISIT_SUM)
            {
                string text = File.ReadAllText("log.txt");
                string line = "[AMP] ITEM UNIQ=`"+item.UNICUM+"` have wrong visit_sum, must be `"+itemFnd.TARIF+"` have `"+item.VISIT_SUM+"`";
                File.WriteAllText("log.txt",text+Environment.NewLine+line);
                
            }
            if (itemFnd.ADD_TARIF != item.ADD_SUM)
            {
                string text = File.ReadAllText("log.txt");
                string line = "[AMP] ITEM UNIQ=`"+item.UNICUM+"` have wrong add_sum, must be `"+itemFnd.ADD_TARIF+"` have `"+item.ADD_SUM+"`";
                File.WriteAllText("log.txt",text+Environment.NewLine+line);
                
            }
           
        }
    }

    public class AMP : DataFile<AMPItem>
    {
        public AMP(string path) : base(path)
        {
        }

        public override AMPItem assignReqToStruct(Dictionary<string, string> req)
        {
            AMPItem returnItem = new AMPItem();

            returnItem.KVR = req["KVR"].Trim(' ');
            returnItem.MKB = req["MKB"].Trim(' ');
            returnItem.VISIT_CODE = req["VISIT_CODE"].Trim(' ');
            returnItem.START_DATE = DateTime.Parse(req["START_DATE"]);
            returnItem.FINAL_DATE = DateTime.Parse(req["FINAL_DATE"]);
            returnItem.AMOUNT = Math.Round(double.Parse(req["AMOUNT"]),2);
            if (req["VISIT_SUM"]!="")
                returnItem.VISIT_SUM = Math.Round( double.Parse(req["VISIT_SUM"]),2);
            if (req["ADD_SUM"]!="")
                returnItem.ADD_SUM =  Math.Round(double.Parse(req["ADD_SUM"]),2);
            returnItem.TARIF_TYPE = Int32.Parse(req["TARIF_TYPE"]);
            returnItem.FIN_LEVEL = Int32.Parse(req["FIN_LEVEL"]);
            returnItem.ADD_CODE = Int32.Parse(req["ADD_CODE"]);
            returnItem.UNICUM = Int32.Parse(req["UNICUM"]);

            returnItem.EVENT = req["EVENT"].Trim(' ');
            returnItem.TARGET = req["TARGET"].Trim(' ');
            returnItem.PLACE = req["PLACE"].Trim(' ');
            returnItem.ILL_CHAR = req["ILL_CHAR"].Trim(' ');
            
            return returnItem;
        }
    }
    public class AMT : DataFile<AMTItem>
    {
        public AMT(string path) : base(path)
        {
        }

        public override AMTItem assignReqToStruct(Dictionary<string, string> req)
        {
            
            //TODO: only needed fields
            AMTItem retItem = new AMTItem();
            retItem.UNICUM = Int32.Parse(req["UNICUM"]);
            retItem.METHOD = req["METHOD"].Trim(' ');

            return retItem;
        }
    }
    
    public class AMPItem : MPItem
    {
        public string EVENT;
        public string TARGET;
        public string PLACE;
        public string ILL_CHAR;
    }

    public class AMTItem : MTItem
    {
        public string METHOD;
    }
}
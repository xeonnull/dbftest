using System;
using System.Collections.Generic;
using System.IO;
using ConsoleApplication1.core.tables;
using ConsoleApplication1.handbook;

namespace ConsoleApplication1.core.groups
{
    public class CGroup
    {
        private MA _ma;
        private MP _mp;
        private CMT _mt;

        private string path;
        private string prefix;
        private PriceC price_c;

        public CGroup(string pathToExctractedData,string prefix,PriceC price)
        {
            path = pathToExctractedData;
            this.prefix = prefix;
            price_c = price;
            _ma = new MA("\\data\\"+prefix+"CMA.dbf");
            _mp = new MP("\\data\\"+prefix+"CMP.dbf");
            _mt = new CMT("\\data\\"+prefix+"CMT.dbf");
        }

        public void ValidateAllItemsPrice(DateTime time)
        {
            foreach (var item in _mp.getItems(new string[,]{}))
            {
                CheckPrice(item);
            }
        }

        private void CheckPrice(MPItem item)
        {
            
            var itemFnd = price_c.getItem(new[,]
            {
                {"PROF_CODE",item.VISIT_CODE},
                {"TARIF_TYPE",item.TARIF_TYPE+""}
            });

            if (itemFnd.TARIF != item.VISIT_SUM)
            {
                string text = File.ReadAllText("log.txt");
                string line = "[CMP] ITEM UNIQ=`" + item.UNICUM + "` have wrong visit_sum, must be `" + itemFnd.TARIF +
                              "` have `" + item.VISIT_SUM + "`";
                File.WriteAllText("log.txt",text+Environment.NewLine+line);
                //Console.WriteLine("[CMP] ITEM UNIQ=`"+item.UNICUM+"` have wrong visit_sum, must be `"+itemFnd.TARIF+"` have `"+item.VISIT_SUM+"`");
            }
            if (itemFnd.ADD_TARIF != item.ADD_SUM)
            {
                string text = File.ReadAllText("log.txt");
                string line = "[CMP] ITEM UNIQ=`"+item.UNICUM+"` have wrong add_sum, must be `"+itemFnd.ADD_TARIF+"` have `"+item.ADD_SUM+"`";
                File.WriteAllText("log.txt",text+Environment.NewLine+line);
               
            }
           
        }
    }

    public class CMT : DataFile<CMTItem>
    {
        public CMT(string path) : base(path)
        {
        }

        public override CMTItem assignReqToStruct(Dictionary<string, string> req)
        {
            throw new System.NotImplementedException();
        }
    }
    public class CMTItem : MTItem
    {
        public string S_TYPE;
        public string EXTRA;
        public string OUTCOME;
    }
}
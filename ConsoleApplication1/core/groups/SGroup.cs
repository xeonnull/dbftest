using System;
using System.Collections.Generic;
using System.IO;
using ConsoleApplication1.core.tables;
using ConsoleApplication1.handbook;

namespace ConsoleApplication1.core.groups
{
    public class SGroup
    {
        private MA _ma;
        private MP _mp;
        private SMT _mt;

        private string path;
        private string prefix;
        private PriceS price_S;

        public SGroup(string pathToExctractedData,string prefix,PriceS price)
        {
            path = pathToExctractedData;
            this.prefix = prefix;
            price_S = price;
            _ma = new MA("\\data\\"+prefix+"SMA.dbf");
            _mp = new MP("\\data\\"+prefix+"SMP.dbf");
            _mt = new SMT("\\data\\"+prefix+"SMT.dbf");
        }

        public void ValidateAllItemsPrice(DateTime time)
        {
            foreach (var item in _mp.getItems(new string[,]{}))
            {
                CheckPrice(item);
            }
        }

        private void CheckPrice(MPItem item)
        {
            
            var itemFnd = price_S.getItem(new[,]
            {
                {"MKB_CODE",item.MKB},
                {"TARIF_TYPE",item.TARIF_TYPE+""},
                {"LEVEL",item.FIN_LEVEL+""},
                {"ADD_CODE",item.ADD_CODE+""}
            });

            if (itemFnd.TARIF != item.VISIT_SUM)
            {
                string text = File.ReadAllText("log.txt");
                string line = "[SMP] ITEM UNIQ=`" + item.UNICUM + "` have wrong visit_sum, must be `" + itemFnd.TARIF +
                              "` have `" + item.VISIT_SUM + "`";
                File.WriteAllText("log.txt",text+Environment.NewLine+line);
                //Console.WriteLine("[CMP] ITEM UNIQ=`"+item.UNICUM+"` have wrong visit_sum, must be `"+itemFnd.TARIF+"` have `"+item.VISIT_SUM+"`");
            }
            if (itemFnd.ADD_TARIF != item.ADD_SUM)
            {
                string text = File.ReadAllText("log.txt");
                string line = "[SMP] ITEM UNIQ=`"+item.UNICUM+"` have wrong add_sum, must be `"+itemFnd.ADD_TARIF+"` have `"+item.ADD_SUM+"`";
                File.WriteAllText("log.txt",text+Environment.NewLine+line);
               
            }
           
        }
    }
    public class SMT : DataFile<SMTItem>
    {
        public SMT(string path) : base(path)
        {
        }

        public override SMTItem assignReqToStruct(Dictionary<string, string> req)
        {
            throw new System.NotImplementedException();
        }
    }
    public class SMTItem : MTItem
    {
        public string S_TYPE;
        public string EXTRA;
        public string OUTCOME;
    }
}
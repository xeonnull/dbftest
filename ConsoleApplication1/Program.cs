﻿using System;
using System.IO;
using System.Threading;
using ConsoleApplication1.core.groups;
using ConsoleApplication1.handbook;
using ConsoleApplication1.helpers;

namespace ConsoleApplication1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            DateTime currDate = DateTime.Parse("2011-11-10");
            
            PriceA price_a = new PriceA("PRICE_A.dbf");
            PriceC price_c = new PriceC("price_c.DBF");
            PriceS price_s = new PriceS("PRICE_S.dbf");

            Zip zip = new Zip();
            zip.UnZip("MGOR01BA.zip", "data");

            if (File.Exists("log.txt"))
            {
                File.Delete("log.txt");
            }
            File.Create("log.txt").Close();
            
            SGroup sgroup = new SGroup("data","GOR01",price_s);
            sgroup.ValidateAllItemsPrice(currDate);
            
            CGroup cgroup = new CGroup("data","GOR01",price_c);
            cgroup.ValidateAllItemsPrice(currDate);
            
            AGroup agroup = new AGroup("data","GOR01",price_a);
            agroup.ValidateAllItemsPrice(currDate);
            
            int i = 1;

          
            Directory.Delete("data", true);

            Console.Read();
        }


        
        
    }
}
using System;
using System.Collections.Generic;
using ConsoleApplication1.core;

namespace ConsoleApplication1.handbook
{
    public class PriceA : DataFile<PriceAItem>
    {
        public PriceA(string path) : base(path)
        {
        }


        public override PriceAItem assignReqToStruct(Dictionary<string, string> req)
        {
            PriceAItem returnItem = new PriceAItem();

            returnItem.SPEC_CODE = req["SPEC_CODE"];
            returnItem.START_DATE = DateTime.Parse(req["START_DATE"]);
            returnItem.FINAL_DATE = DateTime.Parse(req["FINAL_DATE"]);
            returnItem.TARIF = double.Parse(req["TARIF"]);
            if (req["ADD_TARIF"] != "")
            {
                returnItem.ADD_TARIF = double.Parse(req["ADD_TARIF"]);
            }
            else
            {
                returnItem.ADD_TARIF = 0;
            }

            returnItem.TARIF_TYPE = Int32.Parse(req["TARIF_TYPE"]);
            returnItem.LEVEL = Int32.Parse(req["LEVEL"]);
            returnItem.METHOD = req["METHOD"];
            returnItem.ADD_CODE = Int32.Parse(req["ADD_CODE"]);

            return returnItem;
        }
    }

    public struct PriceAItem
    {
        public string SPEC_CODE;
        public DateTime START_DATE;
        public DateTime FINAL_DATE;
        public double TARIF;
        public double ADD_TARIF;
        public int TARIF_TYPE;
        public int LEVEL;
        public string METHOD;
        public int ADD_CODE;
    }
}
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.IO;
using ConsoleApplication1.core;

namespace ConsoleApplication1.handbook
{
    public class PriceS:DataFile<PriceSItem>
    {
        public PriceS(string path) : base(path)
        {
        }

      
    public override PriceSItem assignReqToStruct(Dictionary<string, string> req)
        {
            PriceSItem returnItem = new PriceSItem();

            returnItem.MKB_CODE = req["MKB_CODE"];
            returnItem.START_DATE = DateTime.Parse(req["START_DATE"]);
            returnItem.FINAL_DATE = DateTime.Parse(req["FINAL_DATE"]);
            returnItem.TARIF = double.Parse(req["TARIF"]);
            returnItem.ADD_TARIF = double.Parse(req["ADD_TARIF"]);
            returnItem.TARIF_TYPE = Int32.Parse(req["TARIF_TYPE"]);
            returnItem.LEVEL = Int32.Parse(req["LEVEL"]);
            returnItem.ADD_CODE = Int32.Parse(req["ADD_CODE"]);

            return returnItem;
        }
    }
    
    public struct PriceSItem
    {
        public string MKB_CODE;
        public DateTime START_DATE;
        public DateTime FINAL_DATE;
        public double TARIF;
        public double ADD_TARIF;
        public int TARIF_TYPE;
        public int LEVEL;
        public int ADD_CODE;
    }
}
using System;
using System.Collections.Generic;
using ConsoleApplication1.core;

namespace ConsoleApplication1.handbook
{
    public class PriceC : DataFile<PriceCItem>
    {
        public PriceC(string path) : base(path)
        {
        }
        
       public override PriceCItem assignReqToStruct(Dictionary<string, string> req)
        {
            PriceCItem returnItem = new PriceCItem();

            returnItem.PROF_CODE = req["PROF_CODE"];
            returnItem.START_DATE = DateTime.Parse(req["START_DATE"]);
            returnItem.FINAL_DATE = DateTime.Parse(req["FINAL_DATE"]);
            returnItem.TARIF = double.Parse(req["TARIF"]);
            returnItem.ADD_TARIF = double.Parse(req["ADD_TARIF"]);
            returnItem.TARIF_TYPE = Int32.Parse(req["TARIF_TYPE"]);

            return returnItem;
        }
    }

    public struct PriceCItem
    {
        public string PROF_CODE;
        public DateTime START_DATE;
        public DateTime FINAL_DATE;
        public double TARIF;
        public double ADD_TARIF;
        public int TARIF_TYPE;
    }
}

using System;
using System.IO;
using Ionic.Zip;

namespace ConsoleApplication1.helpers
{
    public class Zip
    {
        public bool UnZip(string archivePath,string directory)
        {
            archivePath = Directory.GetCurrentDirectory() + "\\" + archivePath;
            directory = Directory.GetCurrentDirectory() + "\\" + directory;
            try
            {
                using (ZipFile zip = ZipFile.Read(archivePath))
                {
                    foreach (ZipEntry entry in zip)
                    {
                        entry.Extract(directory, ExtractExistingFileAction.OverwriteSilently);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
                return false;
            }
            return true;
        }
    }
}